package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

type User struct {
	Name   string `json:name`
	Email  string `json:email`
	Pass   string `json:pass`
	Gender string `json:gender`
}

var errorBody string = `{"Status 400":"Invalid Body"}`

var errorHeader string = `{"Status 400":"Invalid Header"}`

var success string = `{"Status 200":"Success}`

var listUsers []User = []User{}

func main() {
	StartServer()
}

// Function to Start the server
func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	r := mux.NewRouter()

	r.HandleFunc("/register", Register).Methods("POST").HeadersRegexp("Content-Type", "application/json")
	r.HandleFunc("/login", Login).Methods("POST").HeadersRegexp("Content-Type", "application/json")

	server := &http.Server{
		Addr:        "192.168.0.18:8082",
		IdleTimeout: duration,
		Handler:     r,
	}

	log.Print(server.ListenAndServe())
}

// Function that recive a Request, deal with the errors if necessery, and send a Response
func Register(res http.ResponseWriter, req *http.Request) {

	var u User

	body, _ := ioutil.ReadAll(req.Body)
	json.Unmarshal(body, &u)

	err := json.Unmarshal(body, &u)
	if err != nil {
		res.Write([]byte(errorBody))
		log.Println(err)
	}

	if u.Name == "" || u.Email == "" || u.Pass == "" || u.Gender == "" {
		res.Write([]byte(errorBody))

	} else {
		listUsers = append(listUsers, u)
		res.Write([]byte(success))
	}

	res.Write([]byte(body))
}

// Function that recive a Request, deal with the errors if necessery, and send a Response
func Login(res http.ResponseWriter, req *http.Request) {

	var u User

	body, _ := ioutil.ReadAll(req.Body)
	json.Unmarshal(body, &u)

	err := json.Unmarshal(body, &u)
	if err != nil {
		log.Println(err)
	}

	if u.Name == "" || u.Pass == "" {
		res.Write([]byte(errorBody))

	} else {
		listUsers = append(listUsers, u)
		res.Write([]byte(success))
	}
}
