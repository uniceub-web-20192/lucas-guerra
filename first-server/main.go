package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

func main() {

	StartServer()

	// Essa linha deve ser executada sem alteração
	// da função StartServer
	// log.Println("[INFO] Servidor no ar!")
}

func cebolas(w http.ResponseWriter, r *http.Request) {
	url := fmt.Sprintf("%v %v", r.Method, r.URL)
	w.Write([]byte(url))
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
		// hostname -I
		Addr:        "172.30.5.242:8082",
		IdleTimeout: duration,
	}

	http.HandleFunc("/", cebolas)

	log.Print(server.ListenAndServe())
}
