package main

import (
	"io"
	"net/http"
	"os"
	"time"
)

func main() {

	duration, _ := time.ParseDuration("1000ns")

	// ifconfig (inet)
	// hostname -I

	server := &http.Server{
		Addr:        "172.30.5.242:8082",
		IdleTimeout: duration,
		//	Handler    :
	}

	http.HandleFunc("/cebola", cebolas)

	server.ListenAndServe()
}

func cebolas(w http.ResponseWriter, r *http.Request) {

	//w.Write([]byte("Cebolas são legais"))

	img, _ := os.Open("cebola.jpg")

	defer img.Close()
	w.Header().Set("Content-Type", "image/jpeg")
	io.Copy(w, img)
}
